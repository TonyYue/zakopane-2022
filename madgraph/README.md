# MadGraph Tutorial

## THURSDAY

Contact: Andrew Lifson

This tutorial has multiple sections:
 * [installation/basic command](madgraph/installation)
 * [Learning MG5aMC](https://cp3.irmp.ucl.ac.be/projects/madgraph/attachment/wiki/Milan/Milan_tuto_I.2.pdf)
 * [Loop-Induced](https://cp3.irmp.ucl.ac.be/projects/madgraph/attachment/wiki/Milan/Milan_tuto_loop.pdf)
 * [NLO](https://cp3.irmp.ucl.ac.be/projects/madgraph/attachment/wiki/Milan/tutorial-unimi-2019-NLO.pdf)
 * [Merging](https://cp3.irmp.ucl.ac.be/projects/madgraph/attachment/wiki/Milan/Milan_tuto_merging.pdf)

 If additional theory information are required please check [here](https://cp3.irmp.ucl.ac.be/projects/madgraph/wiki/Milan)

