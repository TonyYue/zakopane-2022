# Sherpa Tutorial

## MONDAY & TUESDAY

Contact: Alan Price, Daniel Reichelt

The instructions can be found in the worksheet (sherpa.pdf) above.

First work through the example for a first Sherpa run.

The advanced options afterwards should give you some ideas for your project.

